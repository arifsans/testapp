class UserData {
  int id;
  String email;
  String firstName;
  String lastName;
  String avatar;

  UserData(this.id, this.email, this.firstName, this.lastName, this.avatar);
}
