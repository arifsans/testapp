import 'package:testapp/helper/networkutil.dart';

class ApiService {
  String baseUrl = "https://reqres.in/api/";

  NetworkUtil netUtil = new NetworkUtil();

  Map<String, String> requestHeaders = {
    'Content-Type': 'application/json',
  };
}
