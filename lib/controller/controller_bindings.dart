import 'package:get/get.dart';
import 'package:testapp/controller/controller_dependency.dart';

class ControllerBindings extends Bindings {
  @override
  void dependencies() {
    bindingSplashScreenController();
    bindingWelcomerScreenController();
    bindingSignUpScreenController();
    bindingSignInScreenController();
    bindingHomeScreenController();
  }
}
