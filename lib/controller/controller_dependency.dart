import 'package:get/get.dart';
import 'package:testapp/pages/homescreen/homescreencontroller.dart';
import 'package:testapp/pages/signinscreen/signinscreencontroller.dart';
import 'package:testapp/pages/signupscreen/signupscreencontroller.dart';
import 'package:testapp/pages/splashscreen/splashscreencontroller.dart';
import 'package:testapp/pages/welcomerscreen/welcomerscreencontroller.dart';

void bindingSplashScreenController() =>
    Get.lazyPut<SplashScreenController>(() => SplashScreenController());

void bindingWelcomerScreenController() =>
    Get.lazyPut<WelcomerScreenController>(() => WelcomerScreenController());

void bindingSignUpScreenController() =>
    Get.lazyPut<SignUpScreenController>(() => SignUpScreenController());

void bindingSignInScreenController() =>
    Get.lazyPut<SignInSceenController>(() => SignInSceenController());

void bindingHomeScreenController() =>
 Get.lazyPut<HomeScreenController>(() => HomeScreenController());
