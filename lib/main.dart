import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:testapp/controller/controller_bindings.dart';
import 'package:testapp/pages/homescreen/homescreen.dart';
import 'package:testapp/pages/signinscreen/signinscreen.dart';
import 'package:testapp/pages/signupscreen/signupscreen.dart';
import 'package:testapp/pages/splashscreen/splashscreen.dart';
import 'package:testapp/pages/welcomerscreen/welcomerscreen.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(MyApp());
}

Map<int, Color> color = {
  50: Color.fromRGBO(255, 185, 0, .1),
  100: Color.fromRGBO(255, 185, 0, .2),
  200: Color.fromRGBO(255, 185, 0, .3),
  300: Color.fromRGBO(255, 185, 0, .4),
  400: Color.fromRGBO(255, 185, 0, .5),
  500: Color.fromRGBO(255, 185, 0, .6),
  600: Color.fromRGBO(255, 185, 0, .7),
  700: Color.fromRGBO(255, 185, 0, .8),
  800: Color.fromRGBO(255, 185, 0, .9),
  900: Color.fromRGBO(255, 185, 0, 1),
};

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MaterialColor colorCustom = MaterialColor(0xffffb900, color);
    return GetMaterialApp(
      title: 'Test Apps',
      theme: ThemeData(
        fontFamily: 'Poppins',
        primarySwatch: colorCustom,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      getPages: [
        GetPage(
          name: '/',
          page: () => SplashScreen(),
          binding: ControllerBindings(),
        ),
        GetPage(
          name: '/welcomerscreen',
          page: () => WelcomerScreen(),
          binding: ControllerBindings(),
        ),
        GetPage(
          name: '/signupscreen',
          page: () => SignUpScreen(),
          binding: ControllerBindings(),
        ),
        GetPage(
          name: '/signinscreen',
          page: () => SignInScreen(),
          binding: ControllerBindings(),
        ),
        GetPage(
          name: '/homescreen',
          page: () => HomeScreen(),
          binding: ControllerBindings(),
        ),
      ],
    );
  }
}
