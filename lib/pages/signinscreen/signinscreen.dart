import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:get/get.dart';
import 'package:testapp/pages/signinscreen/signinscreencontroller.dart';

class SignInScreen extends StatelessWidget {
  final SignInSceenController _signInSceenController =
      Get.find<SignInSceenController>();
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Container(
            height: height,
            width: width,
            color: Color(0xffffb900),
            padding: EdgeInsets.fromLTRB(
                width * 0.05, height * 0.06, width * 0.05, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      child: Icon(Icons.arrow_back),
                      onTap: () {
                        Get.back();
                      },
                    ),
                    GestureDetector(
                      child: Text(
                        "Register",
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      onTap: () {
                        Get.offAndToNamed('/signupscreen');
                      },
                    )
                  ],
                ),
                SizedBox(height: height * 0.06),
                Text(
                  "Sign In",
                  style: TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: height * 0.01),
                Text(
                  "Masuk dan jelajahi aplikasi untuk mendapatkan keuntungan lainnya",
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                  ),
                )
              ],
            ),
          ),
          Container(
            height: height * 0.65,
            width: width,
            padding: EdgeInsets.fromLTRB(
                width * 0.05, height * 0.1, width * 0.05, 0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.vertical(
                top: Radius.circular(
                  width * 0.1,
                ),
              ),
              color: Colors.white,
            ),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  TextField(
                    controller: _signInSceenController.email.value,
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                    ),
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(20),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(width * 0.08),
                          ),
                        ),
                        filled: true,
                        hintStyle: TextStyle(
                          fontSize: 14,
                          color: Colors.grey[800],
                        ),
                        hintText: "Email",
                        fillColor: Colors.white70),
                  ),
                  SizedBox(height: height * 0.05),
                  TextField(
                    controller: _signInSceenController.password.value,
                    obscureText: true,
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                    ),
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(20),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(width * 0.08),
                          ),
                        ),
                        filled: true,
                        hintStyle: TextStyle(
                          fontSize: 14,
                          color: Colors.grey[800],
                        ),
                        hintText: "Password",
                        fillColor: Colors.white70),
                  ),
                  SizedBox(height: height * 0.1),
                  FlatButton(
                    height: height * 0.1,
                    minWidth: width,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(width * 0.1),
                    ),
                    onPressed: () {
                      Timer(Duration(seconds: 2), () {
                        _signInSceenController.signIn(
                            _signInSceenController.email.value.text,
                            _signInSceenController.password.value.text);
                      });
                      return Get.dialog(
                        SpinKitWave(
                          color: Color(0xffffb900),
                          size: 30,
                          duration: Duration(seconds: 1),
                        ),
                      );
                    },
                    child: Text(
                      "Sign In",
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    color: Colors.black,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
