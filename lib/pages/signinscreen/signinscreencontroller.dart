import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:testapp/helper/model/SignInFailedModel.dart';
import 'package:testapp/helper/model/SignInModel.dart';
import 'package:testapp/helper/apiservice.dart';

class SignInSceenController extends GetxController {
  var loginResult = ''.obs;
  var email = TextEditingController().obs;
  var password = TextEditingController().obs;

  ///Function for login
  Future signIn(String email, String password) async {
    return await ApiService().netUtil.post(
      ApiService().baseUrl.toString() + "login",
      body: {
        'email': email,
        'password': password,
      },
    ).then(
      (dynamic res) {
        Get.back();
        if (res.toString().toLowerCase().contains('error')) {
          SignInFailedModel signInResponse = SignInFailedModel.fromJson(res);
          loginResult.value = signInResponse.error;
          Get.defaultDialog(
            title: "Message",
            titleStyle: TextStyle(fontSize: 20),
            content: Container(
              height: 100,
              width: 300,
              child: Center(
                child: Text(loginResult.value),
              ),
            ),
          );
          print(loginResult.value);
        } else {
          SignInModel signInResponse = SignInModel.fromJson(res);
          loginResult.value = signInResponse.token;
          Get.toNamed('/homescreen');
          print(loginResult.value);
        }
      },
    );
  }
}
