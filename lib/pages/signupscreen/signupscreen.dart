import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:testapp/pages/signupscreen/signupscreencontroller.dart';

class SignUpScreen extends StatelessWidget {
  final SignUpScreenController _signUpScreenController =
      Get.find<SignUpScreenController>();
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Container(
            height: height,
            width: width,
            color: Color(0xffffb900),
            padding: EdgeInsets.fromLTRB(
                width * 0.05, height * 0.06, width * 0.05, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      child: Icon(Icons.arrow_back),
                      onTap: () {
                        Get.back();
                      },
                    ),
                    GestureDetector(
                      child: Text(
                        "Login",
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      onTap: () {
                        Get.offAndToNamed('/signinscreen');
                      },
                    )
                  ],
                ),
                SizedBox(height: height * 0.06),
                Text(
                  "Sign Up",
                  style: TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: height * 0.01),
                Text(
                  "Daftar untuk masuk ke aplikasi dan mendapatkan keuntungan",
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                  ),
                )
              ],
            ),
          ),
          Container(
            height: height * 0.65,
            width: width,
            padding: EdgeInsets.fromLTRB(
                width * 0.05, height * 0.1, width * 0.05, 0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.vertical(
                top: Radius.circular(
                  width * 0.1,
                ),
              ),
              color: Colors.white,
            ),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  TextField(
                    controller: _signUpScreenController.email.value,
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                    ),
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(20),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(width * 0.08),
                          ),
                        ),
                        filled: true,
                        hintStyle: TextStyle(
                          fontSize: 14,
                          color: Colors.grey[800],
                        ),
                        hintText: "Email",
                        fillColor: Colors.white70),
                  ),
                  SizedBox(height: height * 0.05),
                  TextField(
                    controller: _signUpScreenController.password.value,
                    obscureText: true,
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                    ),
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(20),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(width * 0.08),
                          ),
                        ),
                        filled: true,
                        hintStyle: TextStyle(
                          fontSize: 14,
                          color: Colors.grey[800],
                        ),
                        hintText: "Password",
                        fillColor: Colors.white70),
                  ),
                  SizedBox(height: height * 0.1),
                  FlatButton(
                    height: height * 0.1,
                    minWidth: width,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(width * 0.1),
                    ),
                    onPressed: () {
                      Timer(Duration(seconds: 2), () {
                        _signUpScreenController.signUp(
                            _signUpScreenController.email.value.text,
                            _signUpScreenController.password.value.text);
                      });
                      return Get.dialog(
                        SpinKitWave(
                          color: Color(0xffffb900),
                          size: 30,
                          duration: Duration(seconds: 1),
                        ),
                      );
                    },
                    child: Text(
                      "Sign Up",
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    color: Colors.black,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
