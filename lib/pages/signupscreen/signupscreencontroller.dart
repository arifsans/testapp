import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:testapp/helper/apiservice.dart';
import 'package:testapp/helper/model/SignUpFailedModel.dart';
import 'package:testapp/helper/model/SignUpModel.dart';

class SignUpScreenController extends GetxController {
  var email = TextEditingController().obs;
  var password = TextEditingController().obs;
  var registerResult = ''.obs;

  ///Function for register
  Future signUp(String email, String password) async {
    return await ApiService().netUtil.post(
      ApiService().baseUrl.toString() + "register",
      body: {
        'email': email,
        'password': password,
      },
    ).then(
      (dynamic res) {
        Get.back();
        if (res.toString().toLowerCase().contains('error')) {
          SignUpFailedModel registerResponse = SignUpFailedModel.fromJson(res);
          registerResult.value = registerResponse.error;
          Get.defaultDialog(
            title: "Message",
            titleStyle: TextStyle(fontSize: 20),
            content: Container(
              height: 100,
              width: 300,
              child: Center(
                child: Text(registerResult.value),
              ),
            ),
          );
          print(registerResult.value);
        } else {
          SignUpModel registerResponse = SignUpModel.fromJson(res);
          registerResult.value = registerResponse.token;
          Get.offAndToNamed('/signinscreen');
          print(registerResult.value);
        }
      },
    );
  }
}
