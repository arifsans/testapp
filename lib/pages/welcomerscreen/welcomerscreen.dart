import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:testapp/pages/welcomerscreen/welcomerscreencontroller.dart';

class WelcomerScreen extends StatelessWidget {
  final WelcomerScreenController _welcomerScreenController =
      Get.find<WelcomerScreenController>();
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Container(
            height: height,
            width: width,
            padding: EdgeInsets.only(top: height * 0.1),
            child: Column(
              children: [
                Image.asset(
                  "assets/images/logo.png",
                  width: width * 0.6,
                ),
              ],
            ),
          ),
          Container(
            height: height * 0.45,
            width: width,
            padding: EdgeInsets.all(width * 0.05),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.vertical(
                top: Radius.circular(width * 0.1),
              ),
              color: Color(0xffffb900),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Welcome",
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: height * 0.05,
                ),
                Text(
                  "Mulai Aplikasi Sekarang Juga Untuk Mendapatkan Berbagai Keuntungan Dengan Bergabung Di Aplikasi",
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(
                  height: height * 0.05,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    FlatButton(
                      height: height * 0.08,
                      minWidth: width * 0.3,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(width * 0.1),
                      ),
                      onPressed: () {
                        ///Navigate to sign in screen
                        Get.toNamed('/signinscreen');
                      },
                      child: Text(
                        "Sign In",
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      color: Colors.black,
                    ),
                    SizedBox(
                      width: width * 0.05,
                    ),
                    FlatButton(
                      height: height * 0.08,
                      minWidth: width * 0.3,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(width * 0.1),
                      ),
                      onPressed: () {
                        ///Navigate to sign up screen
                        Get.toNamed('/signupscreen');
                      },
                      child: Text(
                        "Sign Up",
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.black,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      color: Colors.white,
                    ),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
