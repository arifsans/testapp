import 'dart:async';

import 'package:get/get.dart';

class SplashScreenController extends GetxController {
  @override
  void onInit() {
    startSplashScreen();
    super.onInit();
  }

  ///Move to Welcomer Screen
  startSplashScreen() async {
    Timer(
      Duration(seconds: 2),
      () => Get.offNamed('/welcomerscreen'),
    );
  }
}
