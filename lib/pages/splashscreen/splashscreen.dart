import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:testapp/pages/splashscreen/splashscreencontroller.dart';

class SplashScreen extends StatelessWidget {
  final SplashScreenController _splashScreenController =
      Get.find<SplashScreenController>();
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
        height: height,
        width: width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Align(
              child: Container(
                height: width * 0.35,
                width: width * 0.35,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(width * 0.5),
                  ),
                  color: Color(0xffffb900),
                ),
              ),
              alignment: Alignment.topRight,
            ),
            Image.asset(
              "assets/images/logo.png",
              width: width * 0.6,
            ),
            Align(
              child: Container(
                height: width * 0.5,
                width: width * 0.5,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(width * 0.5),
                  ),
                  color: Color(0xffffb900),
                ),
              ),
              alignment: Alignment.bottomLeft,
            ),
          ],
        ),
      ),
    );
  }
}
