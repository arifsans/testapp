import 'package:flutter/material.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:testapp/pages/homescreen/homescreencontroller.dart';

class HomeScreen extends StatelessWidget {
  final HomeScreenController _homeScreenController =
      Get.find<HomeScreenController>();
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: OfflineBuilder(
        connectivityBuilder: (
          BuildContext context,
          ConnectivityResult connectivity,
          Widget child,
        ) {
          ///Check if the connectivity is available
          final bool connected = connectivity != ConnectivityResult.none;
          return Stack(
            alignment: Alignment.center,
            children: [
              Container(
                height: height,
                width: width,
                color: Color(0xffffb900),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Align(
                      child: Container(
                        height: width * 0.35,
                        width: width * 0.35,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(width * 0.5),
                          ),
                          color: Colors.white,
                        ),
                      ),
                      alignment: Alignment.topRight,
                    ),
                    Align(
                      child: Container(
                        height: width * 0.5,
                        width: width * 0.5,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(width * 0.5),
                          ),
                          color: Colors.white,
                        ),
                      ),
                      alignment: Alignment.bottomLeft,
                    ),
                  ],
                ),
              ),
              Container(
                height: height,
                width: width,
                padding: EdgeInsets.symmetric(horizontal: width * 0.05),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Obx(
                      () => _homeScreenController.userList.isEmpty
                          ? Center(
                              child: SpinKitWave(
                                color: Colors.white,
                                size: 30,
                                duration: Duration(seconds: 1),
                              ),
                            )
                          : Obx(
                              ///Show list of user data
                              () => ListView.separated(
                                  padding: EdgeInsets.zero,
                                  shrinkWrap: true,
                                  itemBuilder: (BuildContext context, index) {
                                    return GestureDetector(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Obx(
                                            () => CircleAvatar(
                                              backgroundImage: NetworkImage(
                                                  _homeScreenController
                                                      .userList[index].avatar),
                                              radius: width * 0.06,
                                            ),
                                          ),
                                          SizedBox(
                                            width: width * 0.05,
                                          ),
                                          Container(
                                            width: width * 0.55,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Obx(
                                                  () => Text(
                                                      "${_homeScreenController.userList[index].firstName} ${_homeScreenController.userList[index].lastName}"),
                                                ),
                                                Obx(
                                                  () => Text(
                                                      _homeScreenController
                                                          .userList[index]
                                                          .email),
                                                )
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                      onTap: () {
                                        Get.bottomSheet(userDetailsheet(
                                            height,
                                            width,
                                            _homeScreenController
                                                .userList[index].avatar,
                                            "${_homeScreenController.userList[index].firstName} ${_homeScreenController.userList[index].lastName}",
                                            _homeScreenController
                                                .userList[index].email));
                                      },
                                    );
                                  },
                                  separatorBuilder:
                                      (BuildContext context, index) {
                                    return SizedBox(
                                      height: height * 0.05,
                                    );
                                  },
                                  itemCount:
                                      _homeScreenController.userList.length),
                            ),
                    ),
                    SizedBox(height: height * 0.02),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        /// load previous page
                        GestureDetector(
                          child: Obx(
                            () => Icon(
                              Icons.arrow_left,
                              size: 50,
                              color: _homeScreenController.page.value == 1
                                  ? Colors.white
                                  : Colors.black,
                            ),
                          ),
                          onTap: () {
                            if (connected == true) {
                              _homeScreenController.decrementPage();
                              _homeScreenController.loadUser(
                                _homeScreenController.page.value.toString(),
                              );
                            } else {
                              Get.snackbar(
                                'title',
                                'message',
                                titleText: Container(),
                                messageText: Text(
                                  "No Connection Available",
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                                animationDuration: Duration(seconds: 1),
                                snackPosition: SnackPosition.BOTTOM,
                                backgroundColor: Colors.black,
                              );
                            }
                          },
                        ),

                        /// load next page
                        GestureDetector(
                          child: Obx(
                            () => Icon(
                              Icons.arrow_right,
                              size: 50,
                              color: _homeScreenController.page.value == 2
                                  ? Colors.white
                                  : Colors.black,
                            ),
                          ),
                          onTap: () {
                            if (connected == true) {
                              _homeScreenController.incrementPage();
                              _homeScreenController.loadUser(
                                _homeScreenController.page.value.toString(),
                              );
                            } else {
                              Get.snackbar(
                                'title',
                                'message',
                                titleText: Container(),
                                messageText: Text(
                                  "No Connection Available",
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                                animationDuration: Duration(seconds: 1),
                                snackPosition: SnackPosition.BOTTOM,
                                backgroundColor: Colors.black,
                              );
                            }
                          },
                        ),
                      ],
                    )
                  ],
                ),
              ),
              connected
                  ? Container()
                  : Positioned(
                      top: height * 0.05,
                      child: Container(
                        height: height * 0.05,
                        width: width,
                        color: Color(0xFFEE4400),
                        child: Center(
                          child: Text(
                            "No Internet :( ",
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    )
            ],
          );
        },
        child: Container(),
      ),
    );
  }
}

Widget userDetailsheet(height, width, image, name, email) {
  return Container(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.vertical(
        top: Radius.circular(width * 0.05),
      ),
      color: Colors.white,
    ),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        CircleAvatar(
          backgroundImage: NetworkImage(image),
          radius: width * 0.15,
        ),
        SizedBox(height: height * 0.05),
        Text(
          "Hi!, My Name Is $name,\nYou can call me ${name.toString().split(" ")[0]}.",
          textAlign: TextAlign.center,
        ),
        SizedBox(height: height * 0.05),
        Text(
          "If you need any help, Just contact me on $email .",
          textAlign: TextAlign.center,
        ),
      ],
    ),
  );
}
