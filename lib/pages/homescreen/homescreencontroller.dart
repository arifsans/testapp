import 'dart:async';

import 'package:get/get.dart';
import 'package:testapp/helper/apiservice.dart';
import 'package:testapp/helper/model/UserData.dart';
import 'package:testapp/helper/model/UserModel.dart';

class HomeScreenController extends GetxController {
  var userList = [].obs;
  var totalPages = 0.obs;
  var page = 0.obs;
  var isDeviceConnected = false.obs;

  @override
  void onInit() {
    loadUser("1");
    super.onInit();
  }

  ///Function to load previous page
  incrementPage() {
    page.value < totalPages.value ? page.value++ : page.value = page.value;
  }

  ///Function to load next page
  decrementPage() {
    page.value > 1 ? page.value-- : page.value = page.value;
  }

  ///Function to load data user
  Future loadUser(String user) async {
    return await ApiService()
        .netUtil
        .get(
          ApiService().baseUrl.toString() + "users?page=" + user,
        )
        .then(
      (dynamic res) {
        userList.clear();
        UserModel userResponse = UserModel.fromJson(res);
        page.value = userResponse.page;
        totalPages.value = userResponse.totalPages;
        for (int i = 0; i < userResponse.data.length; i++) {
          userList.add(
            UserData(
              userResponse.data[i].id,
              userResponse.data[i].email,
              userResponse.data[i].firstName,
              userResponse.data[i].lastName,
              userResponse.data[i].avatar,
            ),
          );
        }
      },
    );
  }
}
