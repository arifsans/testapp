# Test App For PT Jeli Cipta Teknologi

Created By Fauzan Arif Sani On 11 - March - 2021

  - Mobile app with flutter framework
  - Using GetX State Management

# ScreenShot

  ![alt tag](https://imgur.com/oPb4fXH.png=40x20)
  ![alt tag](https://imgur.com/6VNrlc8.png=40x20)
  ![alt tag](https://imgur.com/Qpx3kKE.png=40x20)
  ![alt tag](https://imgur.com/pDUUWjS.png=40x20)
  ![alt tag](https://imgur.com/p4WuEQV.png=40x20)
  ![alt tag](https://imgur.com/nLPTbMa.png=40x20)
